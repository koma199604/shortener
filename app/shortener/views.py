from django.conf import settings
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render

from .models import Url


def list(request):
    try:
        urls = Url.objects.all().values('url', 'url_hash')
        for url in urls:
            url['short_url'] = settings.HOST_NAME + url['url_hash']
    except Exception:
        raise Http404("Question does not exist")

    return render(request, 'shortener/list.html', {'urls': urls})


def index(request):
    params = None
    if request.method == 'POST':
        if request.POST.get('origin_uri'):
            origin_uri = request.POST.get('origin_uri')
            try:
                url = Url.objects.get(url=origin_uri)
            except Exception:
                url = Url(url=origin_uri)
                url.save()
            params = {
                'origin_uri': origin_uri,
                'short_url': settings.HOST_NAME + url.url_hash
            }
        else:
            params = {
                'error': 'Please input URL'
            }
    return render(request, 'shortener/index.html', params)


def url_view(request, url_hash):
    try:
        url = Url.objects.get(url_hash=url_hash).url
        return HttpResponseRedirect(url)
    except Exception:
        pass

    return HttpResponseRedirect('/')
