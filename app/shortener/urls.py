from django.urls import path, re_path

from .views import list, index, url_view

urlpatterns = [
    path('list', list),
    path('', index),
    re_path(r'^(?P<url_hash>.+)$', url_view)
]
