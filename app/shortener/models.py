import base64
import uuid

from django.db import models
from django.core.validators import URLValidator


class Url(models.Model):
    url = models.CharField(max_length=256, validators=[URLValidator()])
    url_hash = models.CharField(max_length=10, unique=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.url_hash = self.generate_hash()
        super(Url, self).save(*args, **kwargs)

    def generate_hash(self):
        while True:
            hash = base64.urlsafe_b64encode(uuid.uuid1().bytes)[:6]
            hash_exist = Url.objects.filter(url_hash=hash)
            if not hash_exist:
                break

        hash = hash.decode('utf-8')

        return hash
