from django.test import TestCase
from shortener.models import Url


class UrlViewTest(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        url = Url.objects.create(url='https://test.com/')
        url.save()

    def test_view_url_exists(self):
        resp = self.client.get('/dsdksldks')
        self.assertEquals(resp.status_code, 302)
        self.assertEquals(resp.url, '/')

    def test_redirect_to_url(self):
        url = Url.objects.get(url='https://test.com/')
        resp = self.client.get('/{}'.format(url.url_hash))
        self.assertEquals(resp.status_code, 302)
        self.assertEquals(resp.url, url.url)


class UrlListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        url = Url.objects.create(url='https://test.com/')
        url.save()

    def test_list_view_exists(self):
        resp = self.client.get('/list')
        self.assertEquals(resp.status_code, 200)


class UrlIndexViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        url = Url.objects.create(url='https://test.com/')
        url.save()

    def test_index_view_exists(self):
        resp = self.client.get('/')
        self.assertEquals(resp.status_code, 200)

    def test_index_view_add_url(self):
        resp = self.client.post('/', {'origin_uri': 'http://test.net/'})
        self.assertEquals(resp.status_code, 200)

    def test_index_view_add_url_not_data(self):
        resp = self.client.post('/')
        self.assertEquals(resp.status_code, 200)

    def test_index_view_add_exist_url(self):
        url = Url.objects.get(url='https://test.com/')
        resp = self.client.post('/', {'origin_uri': 'https://test.com/'})
        url_hash = resp.context['short_url'].split('/')[-1]
        self.assertEquals(resp.status_code, 200)
        self.assertEquals(url_hash, url.url_hash)
