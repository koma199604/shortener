from django.test import TestCase
from shortener.models import Url


class UrlModelTest(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        for i in range(100):
            url = Url.objects.create(url='https://test.com/{}'.format(i))
            url.save()

    def test_url(self):
        url = Url.objects.get(id=1).url
        self.assertEquals(url, 'https://test.com/0')

    def test_length_url_hash(self):
        url=Url.objects.get(id=1)
        self.assertEquals(len(url.url_hash), 6)