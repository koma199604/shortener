**Use project:**

run project in develop mode:

`make test_build_up`

stop project in develop mode:

`make test_down`

run project in production:

`make prod_build_up`

stop project in production:

`make prod_down`
