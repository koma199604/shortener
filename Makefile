prod_build_up: prod_build prod_up

prod_build:
	docker-compose -f docker-compose.prod.yml build

prod_up:
	$(eval include .env.prod)
	docker-compose -f docker-compose.prod.yml up -d
	docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
	docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear
	docker-compose -f docker-compose.prod.yml exec web python manage.py createsuperuser --username $(DJANGO_SUPERUSER_USERNAME) --email $(DJANGO_SUPERUSER_EMAIL) --no-input

prod_down:
	docker-compose -f docker-compose.prod.yml down -v

prod_stop:
	docker-compose -f docker-compose.prod.yml down

dev_build_up: dev_build dev_up

dev_build:
	docker-compose build
 
dev_up:
	$(eval include .env.dev)
	docker-compose up -d
	docker-compose exec web python manage.py migrate --noinput
	docker-compose exec web python manage.py createsuperuser --username $(DJANGO_SUPERUSER_USERNAME) --email $(DJANGO_SUPERUSER_EMAIL) --no-input

dev_down:
	docker-compose down -v

dev_stop:
	docker-compose down

test:
	$(eval include .env.dev)
	docker-compose up -d
	docker-compose exec web coverage run --source='shortener/' \
		--omit='shortener/tests/*','shortener/migrations/*' \
		manage.py test --verbosity 2
	docker-compose exec web coverage report -m
	docker-compose down -v
